/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zbill.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 *
 * @author Admin
 */
public class CameraUtil {

  private static OrthographicCamera camera;

  public static OrthographicCamera getCamera() {
    if (camera == null) {
      init();
    }
    return camera;
  }

  public static void init() {
    if (camera != null) {
      return;
    }
    camera = new OrthographicCamera(112, 80);
  }
}
