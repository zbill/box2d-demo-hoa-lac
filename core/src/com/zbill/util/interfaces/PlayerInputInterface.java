/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zbill.util.interfaces;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

/**
 *
 * @author Admin
 */
public class PlayerInputInterface implements InputProcessor {

  
  boolean jump = false;
  boolean wall = false;
  boolean sliceLeft = false;
  boolean sliceRight = false;
  boolean canSlice = false;
  boolean canDash = false;
  boolean dash = false;
  boolean startDash = false;
  boolean canJoint = false;
  boolean startJoint = false;
  boolean endJoint = false;
  boolean climb = false;
  boolean alreadyClimb = false;
  
  @Override
  public boolean keyDown(int keycode) {
    
    if (keycode == Input.Keys.W) {
      jump = true;
    }

    if (keycode == Input.Keys.A) {
    }

    if (keycode == Input.Keys.D) {
      if (!alreadyClimb) {
        alreadyClimb = true;
        climb = true;
      }
    }

    if (keycode == Input.Keys.Q) {
      sliceLeft = true;
    }

    if (keycode == Input.Keys.E) {
      sliceRight = true;
    }
    return false;
  }

  @Override
  public boolean keyUp(int i) {
    return false;
  }

  @Override
  public boolean keyTyped(char c) {
    return false;
  }

  @Override
  public boolean touchDown(int i, int i1, int i2, int i3) {
    return false;
  }

  @Override
  public boolean touchUp(int i, int i1, int i2, int i3) {
    return false;
  }

  @Override
  public boolean touchDragged(int i, int i1, int i2) {
    return false;
  }

  @Override
  public boolean mouseMoved(int i, int i1) {
    return false;
  }

  @Override
  public boolean scrolled(int i) {
    return false;
  }

}
