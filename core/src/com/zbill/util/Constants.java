/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zbill.util;

/**
 *
 * @author Admin
 */
public class Constants {
  public class Game {

    public static final int FRAME_WIDTH = 1024;
    public static final int FRAME_HEIGHT = 720;
    public static final String TITLE = "game";
    public static final boolean DEBUG = true;
    public static final boolean DRAW_PHYSICS_DEBUG = true;
  }
}
