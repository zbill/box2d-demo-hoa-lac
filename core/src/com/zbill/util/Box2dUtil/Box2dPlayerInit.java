/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zbill.util.Box2dUtil;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.zbill.util.PhysicsUtil;

/**
 *
 * @author Admin
 */
public class Box2dPlayerInit {

  private static Fixture playerPhysicsFixture;
  private static Fixture playerSensorFixture;
  static World world = PhysicsUtil.getPhysicsWorld();
  public static Body box;
  private static Body player;
  public static final float maxVelocity = 2f;

  public Body initPlayer() {
    if (player == null) {
      player = createPlayer();
    }
    return player;
  }

  public static Body getPlayer() {
    if(player == null){
      player = createPlayer();
    }
    return player;
  }
  
  public Fixture getPlayerPhysicsFixture() {
    return playerPhysicsFixture;
  }

  public Fixture getPlayerSensorFixture() {
    return playerSensorFixture;
  }

  private static Body createPlayer() {
    BodyDef def = new BodyDef();
    def.type = BodyDef.BodyType.DynamicBody;
    box = world.createBody(def);
//
//    PolygonShape poly = new PolygonShape();
//    poly.setAsBox(0.45f, 1.4f);
//    playerPhysicsFixture = box.createFixture(poly, 1);
//    playerPhysicsFixture.setRestitution(1);
//    playerPhysicsFixture.setDensity(0);
//    poly.dispose();

    CircleShape circle = new CircleShape();
    circle.setRadius(4f);
    circle.setPosition(new Vector2(10, 20));
    playerSensorFixture = box.createFixture(circle, 0);
    playerSensorFixture.setRestitution(0.01f);
    playerSensorFixture.setDensity(0);
    circle.dispose();

//    box.setBullet(true);

    return box;
  }

}
