/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zbill.util.Box2dUtil;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.zbill.util.PhysicsUtil;

/**
 *
 * @author Admin
 */
public class ObjectInit {

  private static final World world = PhysicsUtil.getPhysicsWorld();
  private static Body wallLeft, wallRight, floor, ceiling;

  public static Body createBox(BodyDef.BodyType type, float width, float height, float density) {
    BodyDef def = new BodyDef();
    def.type = type;
    Body box = world.createBody(def);

    PolygonShape poly = new PolygonShape();
    poly.setAsBox(width, height);
    box.createFixture(poly, density);
    poly.dispose();

    return box;
  }

  public static Body createEdge(BodyDef.BodyType type, float x1, float y1, float x2,
    float y2, float density) {
    BodyDef def = new BodyDef();
    def.type = type;
    Body box = world.createBody(def);

    EdgeShape poly = new EdgeShape();
    poly.set(new Vector2(0, 0), new Vector2(x2 - x1, y2 - y1));
    box.createFixture(poly, density);
    box.setTransform(x1, y1, 0);
    poly.dispose();
    return box;
  }

//  public static void initPhysicsWorld() {
//
//    wallLeft = createEdge(BodyDef.BodyType.StaticBody, 0, 0, 0, Constants.Game.FRAME_HEIGHT / 2, 10);
//    wallLeft.setTransform(-200, -200, 0);
//
//    wallRight = createEdge(BodyDef.BodyType.StaticBody, 0, 0, 0, Constants.Game.FRAME_HEIGHT / 2, 10);
//    wallRight.setTransform(200, -200, 0);
//
//    floor = createEdge(BodyDef.BodyType.StaticBody, 0, 0, Constants.Game.FRAME_WIDTH, 0, 10);
//    floor.setTransform(-300, 0, 0);
//
//    ceiling = createBox(BodyDef.BodyType.StaticBody, Constants.Game.FRAME_WIDTH, 10f, 10);
//    ceiling.setTransform(0, 150, 0);
//  }

}
