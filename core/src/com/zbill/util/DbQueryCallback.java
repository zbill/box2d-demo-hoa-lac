/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zbill.util;

import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import javax.swing.text.html.parser.Entity;

/**
 *
 * @author Admin
 */
public class DbQueryCallback implements QueryCallback {

  private Entity entity = null;

  public Entity getEntity() {
    return entity;
  }

  @Override
  public boolean reportFixture(Fixture fixture) {
    entity = (Entity) fixture.getBody().getUserData();
    return false;
  }
}
