/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zbill.util;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.zbill.util.Constants.Game;
import javax.swing.text.html.parser.Entity;

/**
 *
 * @author Admin
 */
public class PhysicsUtil {

  private static World physicsWorld;
  private static Box2DDebugRenderer renderer;

  public static Box2DDebugRenderer getRenderer() {
    if (renderer == null) {
      renderer = new Box2DDebugRenderer();
    }
    return renderer;
  }

  public static World getPhysicsWorld() {
    if (physicsWorld == null) {
      physicsWorld = new World(new Vector2(0, -100), true);
      renderer = new Box2DDebugRenderer();
    }
    return physicsWorld;
  }

  public static Box2DDebugRenderer getDebugRenderer() {
    if (renderer == null) {
      renderer = new Box2DDebugRenderer();
    }
    return renderer;
  }

  public static Entity getAtMouse(float x, float y) {
    DbQueryCallback cb = new DbQueryCallback();
    getPhysicsWorld().QueryAABB(cb, x - 0.01f, y - 0.01f, x + 0.01f, y + 0.01f);
    return cb.getEntity();
  }

  public static void createWorld() {

    Body wallLeft = createEdge(BodyDef.BodyType.StaticBody, 0, 0, 0, Game.FRAME_HEIGHT / 2, 10);
    wallLeft.setTransform(-200, -200, 0);
    System.out.println(wallLeft);
    Body wallRight = createEdge(BodyDef.BodyType.StaticBody, 0, 0, 0, Game.FRAME_HEIGHT / 2, 10);
    wallRight.setTransform(200, -200, 0);

    Body floor = createEdge(BodyDef.BodyType.StaticBody, 0, 0, Game.FRAME_WIDTH, 0, 10);
    floor.setTransform(-300, 0, 0);

    Body ceiling = createBox(BodyDef.BodyType.StaticBody, Game.FRAME_WIDTH, 10f, 10);
    ceiling.setTransform(0, 150, 0);
  }

  public static Body createBox(BodyDef.BodyType type, float width, float height, float density) {
    BodyDef def = new BodyDef();
    def.type = type;
    Body box = getPhysicsWorld().createBody(def);

    PolygonShape poly = new PolygonShape();
    poly.setAsBox(width, height);
    box.createFixture(poly, density);
    poly.dispose();

    return box;
  }

  public static Body createEdge(BodyDef.BodyType type, float x1, float y1, float x2,
    float y2, float density) {
    BodyDef def = new BodyDef();
    def.type = type;
    Body box = getPhysicsWorld().createBody(def);

    EdgeShape poly = new EdgeShape();
    poly.set(new Vector2(0, 0), new Vector2(x2 - x1, y2 - y1));
    box.createFixture(poly, density);
    box.setTransform(x1, y1, 0);
    poly.dispose();
    return box;
  }
}
