/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zbill.me;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import static com.zbill.util.Box2dUtil.Box2dPlayerInit.getPlayer;
import com.zbill.util.CameraUtil;
import static com.zbill.util.CameraUtil.*;
import static com.zbill.util.PhysicsUtil.*;
import com.zbill.util.interfaces.PlayerInputInterface;

/**
 *
 * @author Admin
 */
public class GameScreen extends PlayerInputInterface implements Screen {

  public GameScreen(Game game) {
    CameraUtil.init();
    getPhysicsWorld();
    createWorld();
  }

  @Override
  public void show() {
  }

  @Override
  public void render(float delta) {

    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    getCamera().position.set(getPlayer().getPosition().x + 10, getPlayer().getPosition().y + 10, 0);
    getCamera().update();
    getPhysicsWorld().step(Gdx.graphics.getDeltaTime(), 8, 3);
    getRenderer().render(getPhysicsWorld(), getCamera().combined);

  }

  @Override
  public void resize(int i, int i1) {
  }

  @Override
  public void pause() {
  }

  @Override
  public void resume() {
  }

  @Override
  public void hide() {
  }

  @Override
  public void dispose() {

    getPhysicsWorld().dispose();
    getRenderer().dispose();
  }
  
  

}
